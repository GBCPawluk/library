//
//  ViewController.swift
//  Library
//
//  Created by pawluk on 2016-02-10.
//  Copyright © 2016 pawluk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var book:Book?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        titleLabel.text = book?.title
        authorLabel.text = book?.author
        
        descriptionLabel.text = book?.description
        
        title = book?.title
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func unwindToVC(segue: UIStoryboardSegue) {
    }
    
}

