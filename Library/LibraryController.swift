//
//  LibraryController.swift
//  Library
//
//  Created by pawluk on 2016-02-10.
//  Copyright © 2016 pawluk. All rights reserved.
//

import UIKit


class LibraryController: UITableViewController {
 
    var library = Library()
    
    let CellId = "BookCell"
    let SegueController = "Details"
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return library.books.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellId, forIndexPath: indexPath)
        
        if let book:Book? = library.books[indexPath.row]
        {
            cell.textLabel?.text = book?.title
        }
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SegueController{
            if let indexPath = tableView.indexPathForSelectedRow,
            let destinationController = segue.destinationViewController as? ViewController{
                
                destinationController.book = library.books[indexPath.row]
                
            }
        }
    }
    
    @IBAction func unwindToVC(segue: UIStoryboardSegue) {
    }
    
}
