//
//  Book.swift
//  Library
//
//  Created by pawluk on 2016-02-10.
//  Copyright © 2016 pawluk. All rights reserved.
//

import UIKit

class Book {
    var author: String
    var title: String
    var description: String
    
    init(author: String, title:String, desc: String){
    
        self.author = author
        self.title = title
        self.description = desc
    }
    
}
