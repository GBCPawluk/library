//
//  Library.swift
//  Library
//
//  Created by pawluk on 2016-02-10.
//  Copyright © 2016 pawluk. All rights reserved.
//

import UIKit

class Library {
    
    var books:[Book] = [Book]()
    
    init(){
    
        let path = NSBundle.mainBundle().pathForResource("books", ofType: "plist")
        
        if let fpath = path {
            let collection = NSArray(contentsOfFile: fpath) as! [NSDictionary]
            
            for element in collection {
            
                let book = Book(
                    author: element["Author"] as! String,
                    title: element["Title"] as! String,
                    desc: element["Description"] as! String
                );
                books.append(book)
            }
        }
        
        
    }
}
